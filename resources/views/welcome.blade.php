<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Crud Laravel 8 - Benjamin SH</title>
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" />
       <!-- Bootstrap 5 - benjamin -->
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
   <!-- Bootstrap 5 - benjamin - fin -->
</head>
<body >
    <div class="card text-center">
        <div class="card-body">
          <h5 class="card-title">Desarrollo de Sistemas Orientado a Internet</h5>
          <p class="card-text">Crud Laravel 8 - Benjamin SH.</p>
          <a href="{{ route('posts.index') }}" class="btn btn-primary">Ingresar</a>
        </div>
      </div>
</body>
</html>